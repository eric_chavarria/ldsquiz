module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("olfA");


/***/ }),

/***/ "olfA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
/* harmony default export */ __webpack_exports__["default"] = ((req, res) => {
  res.statusCode = 200;
  res.json({
    "MOBILE": [{
      "id": 1,
      "name": "que es un Lamanita?"
    }, {
      "id": 2,
      "name": "que es el plan de salvacion?"
    }, {
      "id": 3,
      "name": "que es el Libro de mormon?"
    }, {
      "id": 4,
      "name": "que es el sellamiento ?"
    }, {
      "id": 5,
      "name": "que se hace en el templo?"
    }, {
      "id": 6,
      "name": "que hacemos en la santa cena?"
    }, {
      "id": 7,
      "name": "que es la vida eterna?"
    }, {
      "id": 8,
      "name": "que es el sacerdocio?"
    }, {
      "id": 9,
      "name": "por que tengo que volver a bautizarme?"
    }, {
      "id": 10,
      "name": "como realizo el ayuno?"
    }, {
      "id": 11,
      "name": "que es la vida preterrenal?"
    }, {
      "id": 12,
      "name": "que es un profeta?"
    }, {
      "id": 13,
      "name": "como es la trinidad?"
    }, {
      "id": 14,
      "name": "que es doctrina y convenios?"
    }, {
      "id": 15,
      "name": "como encuentro nombres para el templo?"
    }, {
      "id": 16,
      "name": "que es el milenio?"
    }, {
      "id": 17,
      "name": "que nos dice el articulo de fe #4?"
    }, {
      "id": 18,
      "name": "que es la conferencia general?"
    }, {
      "id": 19,
      "name": "que es la palabra de sabiduria?"
    }, {
      "id": 20,
      "name": "que es la ley de castidad?"
    }, {
      "id": 21,
      "name": "como esta compuesta la expiacion?"
    }, {
      "id": 22,
      "name": "que es la apostacia?"
    }, {
      "id": 23,
      "name": "por que nos bautizamos a los 8 ?"
    }, {
      "id": 24,
      "name": "como se tradujo el libro de mormon?"
    }, {
      "id": 25,
      "name": "como se tradjo la perla de gran precio?"
    }, {
      "id": 26,
      "name": "menciona los 10 mandamientos?"
    }, {
      "id": 27,
      "name": "menciona los frutos del espiritu?"
    }, {
      "id": 28,
      "name": "que se conmemora el 6 de abril ?"
    }],
    "WEB": [{
      "id": 0,
      "name": "INE",
      "imgFront": "img/port_ine1.png",
      "text1": "The Mexican Election Government ministry ask us to make a Document repository where they can store and track a huge pdf amount of documents.",
      "text2": "Using Best UI/Ux practices we designed and get up and running a complex pdf repository with a resposive web desing.",
      "text3": null,
      "web": "repositoriodocumental.ine.mx",
      "img1": null,
      "img2": null,
      "img3": null,
      "carousel": ["img/port_ine3.png", "img/port_ine1.png", "img/port_ine2.png"]
    }, {
      "id": 1,
      "name": "Nuvote",
      "imgFront": "img/port_nuvote3.png",
      "text1": "Nuvote LLc restores houses and buildings, they needed a solution to manage work orders and employees, also to hook up with quickbooks service.",
      "text2": "Weused dijango and Oauth2 tech to make it happen supported also with a field app and a huge s3 bucket file storage adn with a Claws setup. this app can manage alot of traffic.",
      "text3": null,
      "web": "nwoms.nuvote.com",
      "img1": null,
      "img2": null,
      "img3": null,
      "carousel": ["img/port_nuvote3.png", "img/port_nuvote.png", "img/port_nuvote2.png"]
    }],
    "GAMES": [{
      "id": 0,
      "name": "Victoria's games",
      "imgFront": "img/port_victoriasgame3.png",
      "text1": "We love gaming at Kondosoft, that's why we decided to make a game inspired by one of our staf members kid.",
      "text2": "Using Phaser and full javascript stack with a heavy development on Canvas and SVG HTML5 capabilities we developed this exciting game for kids to get familiar with voclas.",
      "text3": null,
      "web": null,
      "img1": "img/ic_victoriasgame.png",
      "img2": "img/App_Store_Badge blk.png",
      "img3": null,
      "carousel": ["img/port_victoriasgame3.png", "img/port_victoriasgame.png", "img/port_victoriasgame2.png"]
    }]
  });
});

/***/ })

/******/ });