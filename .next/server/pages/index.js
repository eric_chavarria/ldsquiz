module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("RXBc");


/***/ }),

/***/ "Dtiu":
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ }),

/***/ "F5FC":
/***/ (function(module, exports) {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ "RXBc":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "getStaticProps", function() { return /* binding */ getStaticProps; });

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__("F5FC");

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");

// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__("Dtiu");
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);

// CONCATENATED MODULE: ./src/components/Section/styled.js

const Container = external_styled_components_default.a.section.withConfig({
  displayName: "styled__Container",
  componentId: "sc-1b5rmvd-0"
})(["padding:0 6%;background:linear-gradient(to bottom,", " );color:", ";display:flex;flex-wrap:wrap;align-items:flex-start;justify-content:center;"], ({
  theme
}) => theme.colors.blue, ({
  theme
}) => theme.colors.white);
const Paragraph = external_styled_components_default.a.div.withConfig({
  displayName: "styled__Paragraph",
  componentId: "sc-1b5rmvd-1"
})(["color:rgba(0,66,131,0);font-family:'Raleway';font-weight:600;font-size:3rem;text-align:center;line-height:470px;width:100%;height:100%;transition-property:background;transition-duration:.8s;&:hover{background:rgba(0,66,131,0.7);color:", ";cursor:pointer;}@media (min-width:1184px) and (max-width:1500px){line-height:270px;}@media (min-width:992px) and (max-width:1183px){line-height:270px;}@media (min-width:768px) and (max-width:991.98px){line-height:270px;}@media (min-width:576px) and (max-width:767.98px){line-height:480px;}"], ({
  theme
}) => theme.colors.white);
const Title = external_styled_components_default.a.h2.withConfig({
  displayName: "styled__Title",
  componentId: "sc-1b5rmvd-2"
})(["color:", ";font-family:'Raleway';margin:50px 0;width:100%;line-height:80px;display:flex;justify-content:center;"], ({
  theme
}) => theme.colors.white);
const CardContainer = external_styled_components_default.a.div.withConfig({
  displayName: "styled__CardContainer",
  componentId: "sc-1b5rmvd-3"
})(["max-width:20050px;display:grid;grid-template-columns:repeat(3,500px);grid-template-rows:repeat(", ",500px);@media (min-width:1184px) and (max-width:1500px){max-width:1450px;grid-template-columns:repeat(3,300px);grid-template-rows:repeat(", ",300px);}@media (min-width:992px) and (max-width:1183px){max-width:1450px;grid-template-columns:repeat(3,300px);grid-template-rows:repeat(", ",300px);}@media (min-width:768px) and (max-width:991.98px){max-width:700px;grid-template-columns:repeat(2,345px);grid-template-rows:repeat(", ",300px);}@media (min-width:576px) and (max-width:767.98px){max-width:500px;grid-template-columns:repeat(1,480px);grid-template-rows:repeat(", ",480px);}"], props => props.size > 3 ? '2' : '1', props => props.size > 3 ? '2' : '1', props => props.size > 3 ? '2' : '1', props => props.size > 3 ? '2' : '1', props => props.size);
const Card = external_styled_components_default.a.div.withConfig({
  displayName: "styled__Card",
  componentId: "sc-1b5rmvd-4"
})(["margin:0 10px 20px 10px;background-image:url(", ");background-repeat:no-repeat;background-size:cover;background-position:center;overflow:hidden;-webkit-box-shadow:11px 9px 5px -6px rgba(0,0,0,0.32);-moz-box-shadow:11px 9px 5px -6px rgba(0,0,0,0.32);box-shadow:11px 9px 5px -6px rgba(0,0,0,0.32);"], props => props.image);
const Logo = external_styled_components_default.a.img.withConfig({
  displayName: "styled__Logo",
  componentId: "sc-1b5rmvd-5"
})(["whidth:60px;height:80px;padding-right:10px;@media (min-width:768px) and (max-width:991.98px){width:50px;height:75px;}@media (min-width:576px) and (max-width:767.98px){width:45px;height:75px;}"]);
// CONCATENATED MODULE: ./src/components/Section/index.js


function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }


function Section(_ref) {
  let {
    jdata,
    kid,
    onShowModal
  } = _ref,
      props = _objectWithoutProperties(_ref, ["jdata", "kid", "onShowModal"]);

  return /*#__PURE__*/Object(jsx_runtime_["jsx"])(Container, {
    id: kid,
    children: /*#__PURE__*/Object(jsx_runtime_["jsx"])(CardContainer, {
      size: jdata.length,
      children: jdata.map(item => /*#__PURE__*/Object(jsx_runtime_["jsx"])(Card, {
        image: 'fill2.jpg',
        onClick: onShowModal(jdata[item.id]),
        children: /*#__PURE__*/Object(jsx_runtime_["jsx"])(Paragraph, {
          name: item.id,
          children: item.id
        })
      }, item.id))
    })
  });
}
// CONCATENATED MODULE: ./src/components/Modal/styled.js

const styled_Title = external_styled_components_default.a.p.withConfig({
  displayName: "styled__Title",
  componentId: "gmk54z-0"
})(["color:", ";font-family:'Raleway';font-size:50px;width:10%;height:90%;text-align:center;"], ({
  theme
}) => theme.colors.orange);
const styled_Container = external_styled_components_default.a.div.withConfig({
  displayName: "styled__Container",
  componentId: "gmk54z-1"
})(["width:100%;padding-left:8%;border-left:", " solid;"], ({
  theme
}) => theme.colors.blue);
const styled_Paragraph = external_styled_components_default.a.p.withConfig({
  displayName: "styled__Paragraph",
  componentId: "gmk54z-2"
})(["font-family:'Raleway';font-size:5rem;margin:20% 0;text-align:center;color:", ";"], ({
  theme
}) => theme.colors.blue);
const X = external_styled_components_default()(styled_Paragraph).withConfig({
  displayName: "styled__X",
  componentId: "gmk54z-3"
})(["width:100%;height:30px;padding:0;margin:0;font-size:.9rem;position:relative;top:0;"]);
const Anchor = external_styled_components_default.a.a.withConfig({
  displayName: "styled__Anchor",
  componentId: "gmk54z-4"
})(["margin:0;width:10%;height:80%;"]);
const Modal = external_styled_components_default.a.div.withConfig({
  displayName: "styled__Modal",
  componentId: "gmk54z-5"
})(["max-width:1063px;max-height:554px;padding:2% 3%;padding-bottom:50px;height:500px;width:70%;background:", ";color:#0a0a0a;align-items:center;border-radius:20px;-webkit-box-shadow:11px 9px 5px -6px rgba(0,0,0,0.32);-moz-box-shadow:11px 9px 5px -6px rgba(0,0,0,0.32);box-shadow:11px 9px 5px -6px rgba(0,0,0,0.32);@media (min-width:992px) and (max-width:1183px){width:80%;}@media (min-width:576px) and (max-width:767.98px){width:90%;}"], ({
  theme
}) => theme.colors.white);
const Img = external_styled_components_default.a.img.withConfig({
  displayName: "styled__Img",
  componentId: "gmk54z-6"
})(["width:3%;float:right;"]);
const Btn = external_styled_components_default.a.img.withConfig({
  displayName: "styled__Btn",
  componentId: "gmk54z-7"
})(["width:100%;height:100%;fill:blue !important;"]);
const Carousel = external_styled_components_default.a.img.withConfig({
  displayName: "styled__Carousel",
  componentId: "gmk54z-8"
})(["width:80%;height:80%;"]);
const Box = external_styled_components_default.a.div.withConfig({
  displayName: "styled__Box",
  componentId: "gmk54z-9"
})(["height:90%;width:60%;display:flex;justify-content:space-between;max-width:617px;"]);
const BoxParagraph = external_styled_components_default()(Box).withConfig({
  displayName: "styled__BoxParagraph",
  componentId: "gmk54z-10"
})(["flex-wrap:wrap;align-items:center;width:40%;align-content:space-between;"]);
const Us = external_styled_components_default.a.div.withConfig({
  displayName: "styled__Us",
  componentId: "gmk54z-11"
})(["max-width:253px;width:100%;display:flex;flex-wrap:wrap;margin:3%;justify-content:space-between;"]);
const SubTitle = external_styled_components_default()(styled_Title).withConfig({
  displayName: "styled__SubTitle",
  componentId: "gmk54z-12"
})(["font-size:15px;"]);
const Os = external_styled_components_default.a.img.withConfig({
  displayName: "styled__Os",
  componentId: "gmk54z-13"
})(["width:45%;height:30px;@media (min-width:768px) and (max-width:991.98px){height:20px;}@media (min-width:576px) and (max-width:767.98px){height:20px;}"]);
const styled_Logo = external_styled_components_default.a.img.withConfig({
  displayName: "styled__Logo",
  componentId: "gmk54z-14"
})(["whidth:60px;height:80px;padding-right:10px;@media (min-width:768px) and (max-width:991.98px){width:50px;height:75px;object-fit:scale-down;}@media (min-width:576px) and (max-width:767.98px){width:45px;height:75px;object-fit:scale-down;}"]);
const Border = external_styled_components_default.a.div.withConfig({
  displayName: "styled__Border",
  componentId: "gmk54z-15"
})(["width:100%;border:", " solid;display:flex;align-items:center;padding:10px 20px;position:relative;top:-40px;@media (min-width:576px) and (max-width:767.98px){width:80%;padding:10px 20px 10px 10px;}"], ({
  theme
}) => theme.colors.blue);
const Shadow = external_styled_components_default.a.div.withConfig({
  displayName: "styled__Shadow",
  componentId: "gmk54z-16"
})(["width:100%;height:100%;background:rgba(7,7,6,.3);position:fixed;top:0;visibility:", ";z-index:4;display:flex;justify-content:center;align-items:center"], props => props.hidden);
// CONCATENATED MODULE: ./src/components/Modal/index.js



function Modal_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = Modal_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function Modal_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }


function ModalCard(_ref) {
  let {
    visible,
    selectedProjectId,
    onCloseModal,
    numImage,
    setNumImage
  } = _ref,
      props = Modal_objectWithoutProperties(_ref, ["visible", "selectedProjectId", "onCloseModal", "numImage", "setNumImage"]);

  return /*#__PURE__*/Object(jsx_runtime_["jsx"])(Shadow, {
    hidden: visible,
    children: /*#__PURE__*/Object(jsx_runtime_["jsxs"])(Modal, {
      children: [/*#__PURE__*/Object(jsx_runtime_["jsxs"])(X, {
        children: [/*#__PURE__*/Object(jsx_runtime_["jsx"])(Img, {
          src: "img/icon-close.svg",
          onClick: onCloseModal
        }), " "]
      }), /*#__PURE__*/Object(jsx_runtime_["jsx"])(styled_Paragraph, {
        children: selectedProjectId.name
      })]
    })
  });
}
// CONCATENATED MODULE: ./src/_styled.js

const Projects = external_styled_components_default.a.section.withConfig({
  displayName: "styled__Projects",
  componentId: "sc-1xa3ra9-0"
})(["position:relative;background:", ";color:", ";align-items:center;"], ({
  theme
}) => theme.colors.blue, ({
  theme
}) => theme.colors.white);
const Header = external_styled_components_default()(Projects).withConfig({
  displayName: "styled__Header",
  componentId: "sc-1xa3ra9-1"
})([""]);
const _styled_Paragraph = external_styled_components_default.a.p.withConfig({
  displayName: "styled__Paragraph",
  componentId: "sc-1xa3ra9-2"
})(["padding:0 12%;margin-bottom:4%;font-family:'Raleway';font-size:1.5rem;text-align:center;"]);
const _styled_Title = external_styled_components_default.a.h2.withConfig({
  displayName: "styled__Title",
  componentId: "sc-1xa3ra9-3"
})(["color:", ";padding:5% 12% 3% 12%;font-family:'Raleway';font-size:2.8rem;font-weight:400;text-align:center;margin:0 6%;border-top:1px ", " solid;@media (min-width:768px) and (max-width:991.98px){padding:8% 12% 3% 12%;}@media (min-width:576px) and (max-width:767.98px){padding:12% 12% 3% 12%;}"], ({
  theme
}) => theme.colors.orange, ({
  theme
}) => theme.colors.white);
const Bullet = external_styled_components_default()(_styled_Title).withConfig({
  displayName: "styled__Bullet",
  componentId: "sc-1xa3ra9-4"
})(["padding:2% 0 1% 0;border-style:none;font-size:2.5rem;"]);
const Strong = external_styled_components_default.a.strong.withConfig({
  displayName: "styled__Strong",
  componentId: "sc-1xa3ra9-5"
})(["font-weight:600;"]);
const _styled_Img = external_styled_components_default.a.img.withConfig({
  displayName: "styled__Img",
  componentId: "sc-1xa3ra9-6"
})(["width:100%;object-fit:fill;"]);
const ContainerFixed = external_styled_components_default.a.div.withConfig({
  displayName: "styled__ContainerFixed",
  componentId: "sc-1xa3ra9-7"
})(["position:fixed;width:calc(100% - 40px);bottom:40px;display:flex;justify-content:flex-end;"]);
const Button = external_styled_components_default.a.img.withConfig({
  displayName: "styled__Button",
  componentId: "sc-1xa3ra9-8"
})(["width:50px;"]);
// CONCATENATED MODULE: ./src/pages/index.js








function Home(Info) {
  const {
    0: numImage,
    1: setNumImage
  } = Object(external_react_["useState"])(0);
  const {
    0: hidden,
    1: setHiden
  } = Object(external_react_["useState"])('hidden');
  const {
    0: element,
    1: setElement
  } = Object(external_react_["useState"])('');
  const click = Object(external_react_["useCallback"])(number => {
    return () => {
      setHiden('visible');
      setElement(number);
    };
  }, []);

  function closeModal() {
    setElement('');
    setHiden('hidden');
    setNumImage(0);
  }

  return /*#__PURE__*/Object(jsx_runtime_["jsx"])(jsx_runtime_["Fragment"], {
    children: /*#__PURE__*/Object(jsx_runtime_["jsxs"])(Projects, {
      children: [/*#__PURE__*/Object(jsx_runtime_["jsx"])(Bullet, {
        children: "Preguntas"
      }), /*#__PURE__*/Object(jsx_runtime_["jsx"])(Section, {
        onShowModal: click,
        jdata: Info.Info.MOBILE,
        kid: "MOBILE"
      }), /*#__PURE__*/Object(jsx_runtime_["jsx"])(ModalCard, {
        numImage: numImage,
        setNumImage: setNumImage,
        visible: hidden,
        selectedProjectId: element,
        onCloseModal: closeModal
      })]
    })
  });
}

/* harmony default export */ var pages = __webpack_exports__["default"] = (Home);
async function getStaticProps() {
  const res = await fetch('http://localhost:3000/api/info-pages');
  const Info = await res.json();
  return {
    props: {
      Info
    }
  };
}

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });