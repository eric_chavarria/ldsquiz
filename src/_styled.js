import styled from 'styled-components'

export const Projects = styled.section`
  position: relative;
  background : ${({ theme }) => theme.colors.blue};
  color: ${({ theme }) => theme.colors.white};
  align-items: center;
`
export const Header = styled(Projects)`
  
`
export const Paragraph= styled.p`
  padding: 0 12%;
  margin-bottom: 4%; 
  font-family: 'Raleway';
  font-size: 1.5rem;
  text-align: center;
`
export const Title = styled.h2`
  color: ${({ theme }) => theme.colors.orange};
  padding: 5% 12% 3% 12%;
  font-family: 'Raleway';
  font-size:2.8rem;
  font-weight: 400;
  text-align: center;
  margin: 0 6%;
  border-top: 1px ${({ theme }) => theme.colors.white} solid;
  @media (min-width: 768px) and (max-width: 991.98px) { 
    padding: 8% 12% 3% 12%;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    padding: 12% 12% 3% 12%;
  }
`

export const Bullet = styled(Title)`
  padding:2% 0 1% 0;
  border-style: none;
  font-size: 2.5rem;
`

export const Strong = styled.strong`
font-weight: 600;
 `

export const Img = styled.img`
  width: 100%;
  object-fit: fill;
`
export const ContainerFixed = styled.div`
  position:fixed;
  width: calc(100% - 40px);
  bottom: 40px;
  display:flex;
  justify-content: flex-end;

`
export const Button = styled.img`
  width: 50px;
`
