import styled from 'styled-components'

export const Title = styled.p`
  color: ${({ theme }) => theme.colors.orange};
  font-family: 'Raleway';
  font-size:50px;
   width: 10%;
  height:90%;
  text-align: center;

 
`

export const Container = styled.div`
  width: 100%;
  padding-left: 8%;
  border-left: ${({ theme }) => theme.colors.blue} solid;
`

export const Paragraph = styled.p`
  font-family: 'Raleway';
  font-size: 5rem;
  margin: 20% 0; 
  text-align:center;
color: ${({ theme }) => theme.colors.blue};
`

export const X = styled(Paragraph)`
  width: 100%;
  height: 30px;
  padding: 0;
  margin: 0; 
  font-size: .9rem;
  position: relative;
  top:0;
`

export const Anchor = styled.a`
  margin: 0;
  width:  10%;
  height: 80%;

`

export const Modal = styled.div`
  max-width: 1063px;
  max-height: 554px;
  padding: 2% 3%;
  padding-bottom: 50px;
  height: 500px;
  width: 70%;
  background : ${({ theme }) => theme.colors.white};
  color: #0a0a0a;
  align-items: center;
  border-radius: 20px;
  -webkit-box-shadow: 11px 9px 5px -6px rgba(0,0,0,0.32);
  -moz-box-shadow: 11px 9px 5px -6px rgba(0,0,0,0.32);
  box-shadow: 11px 9px 5px -6px rgba(0,0,0,0.32);
  @media (min-width: 992px) and (max-width: 1183px) { 
    width: 80%;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 90%;
  }
`

export const Img = styled.img`
  width: 3%;
  float: right;
`

export const Btn = styled.img`
  width: 100%;
  height: 100%;
  fill: blue !important;
`

export const Carousel = styled.img`
  width: 80%;
  height: 80%;
`
export const Box = styled.div`
  height: 90%;
  width: 60%;
  display: flex;
  justify-content: space-between;
  max-width: 617px;
`

export const BoxParagraph = styled(Box)`
  flex-wrap: wrap;
  align-items: center;
  width: 40%;
  align-content: space-between;
`

export const Us = styled.div`
 max-width: 253px;
  width: 100%;
   display: flex;
   flex-wrap: wrap;
   margin: 3%;
   justify-content: space-between;
`

export const SubTitle = styled(Title)`
  font-size: 15px;
`

export const Os = styled.img`
  width: 45%;
  height:30px;
   @media (min-width: 768px) and (max-width: 991.98px) {
     height: 20px;
  }
   @media (min-width: 576px) and (max-width: 767.98px) {  
    height: 20px;
  }

`

export const Logo = styled.img`
  whidth: 60px;
  height: 80px;
  padding-right: 10px;
  @media (min-width: 768px) and (max-width: 991.98px) {
     width: 50px;
     height: 75px;
     object-fit: scale-down;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 45px;  
    height: 75px;
   object-fit: scale-down;
  }
`
export const Border = styled.div`
  width: 100%;
  border: ${({ theme }) => theme.colors.blue} solid;
  display: flex;
  align-items: center;
  padding: 10px 20px;
  position: relative;
  top: -40px;
   @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 80%;
    padding: 10px 20px 10px 10px;
  }
`

export const Shadow = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(7,7,6, .3);
  position: fixed;
  top: 0;
  visibility:  ${props => props.hidden};
  z-index: 4;
  display: flex;
  justify-content: center;
  align-items: center
`
