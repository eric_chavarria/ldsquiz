import {
  Title,
  Container,
  Border,
  Paragraph,
  Anchor,
  Modal,
  Img,
  Btn,
  Carousel,
  Box,
  BoxParagraph,
  Us,
  SubTitle,
  Os,
  Shadow,
  X,
  Logo
} from './styled'

export default function ModalCard ({ visible, selectedProjectId, onCloseModal, numImage, setNumImage, ...props }) {
  
  return (
    <Shadow hidden={visible}>
      <Modal>
        <X><Img src='img/icon-close.svg' onClick={onCloseModal} /> </X>
        <Paragraph>{selectedProjectId.name}</Paragraph>
        
      </Modal>
    </Shadow>
  )
}
