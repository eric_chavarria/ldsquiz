import { useState } from 'react'
import {
  Form,
  Container,
  Section,
  Title,
  SubTitle,
  Paragraph,
  InputName,
  InputMail,
  InputSubject,
  InputMessage,
  Button,
  Strong,
  BtnCase
} from './styled'

export default function HireForm () {
  const [data, setData] = useState({
    name: '',
    mail: '',
    subject: '',
    message: ''

  })
  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: [e.target.value]
    })
    console.log(data)
  }
  const href = `mailto:example@email.com?subject=${data.subject}&body=${data.message}`
  return (
    <Form id='HIRE'>
      <Title>HIRE A DEV</Title>
      <Container>
        <SubTitle>
          if you need extra help for your project,
          <Strong>
            we're also available for hiring.
          </Strong>
        </SubTitle>
        <Paragraph>
          complete or improve your development team whit one of our dedicate full-stack developers, ready to cooperate with reponsability and will to learn new experiences.
        </Paragraph>
      </Container>
      <Section>
        <InputName placeholder='Name' name='name' onChange={handleChange} />
        <InputMail placeholder='e-mail' name='mail' onChange={handleChange} />
        <InputSubject placeholder='Subject' name='subject' onChange={handleChange} />
        <InputMessage placeholder='Message' name='message' onChange={handleChange} />
        <BtnCase>
          <Button href={href}>Send</Button>
        </BtnCase>
      </Section>
    </Form>
  )
}
