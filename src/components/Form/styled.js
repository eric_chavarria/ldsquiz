import styled from 'styled-components'

export const Form = styled.section`
  padding: 6%;
  background : #f3f3f3;
  background-image: url("img/img_hiredev.png");
  background-repeat: no-repeat;
  background-position: right;
  background-size: 40% 100%;
  color: #0a0a0a;
  @media (min-width: 992px) and (max-width: 1183px) { 
    padding: 2% 2%;
  }
  @media (min-width: 768px) and (max-width: 991.98px) {
    padding: 2% 2%;
    background-size: 47% 100%;
   }
   @media (min-width: 576px) and (max-width: 767.98px) { 
    padding: 2% 2%;
    background-size: 0;
   }
`

export const Container = styled.div`
  width: 50%;
  padding-left: 3%;
  border-left: ${({ theme }) => theme.colors.orange} solid;
  @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 100%;
   }
`

export const Section = styled.form`
  width: 50%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-content: space-between;
  padding-top: 5%;
  @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 100%;
   }
`

export const Title = styled.h2`
color: ${({ theme }) => theme.colors.orange};
font-family: 'Raleway';
font-size: 2.4rem;
padding-top: 8%;
`

export const Strong = styled.strong`
font-size: 2rem;
font-weight: 600;

`

export const SubTitle = styled.h3`
color: ${({ theme }) => theme.colors.blue};
font-family: 'Raleway';
font-weight: 500;
font-size: 2rem;
margin: 0;
width: 80%;
margin-bottom: 6%;
line-height: 2rem;
`

export const Paragraph = styled.p`
  margin-bottom: 4%; 
  font-family: 'Raleway';
  width: 90%;
  font-size: 2rem;
  padding-bottom: 3%;
  line-height: 2rem;
`

export const InputName = styled.input`
  type: "text";
  width: 44%;
  border: 1px  solid ${({ theme }) => theme.colors.blue};
  padding: 2%;
  margin-bottom: 30px;
`

export const InputMail = styled(InputName)`
  type: "email";
  height: 20px;
`

export const InputSubject = styled(InputName)`
  width: 100%;
`

export const InputMessage = styled.textarea`
  type: "text";
  width: 100%;
  border: 1px  solid ${({ theme }) => theme.colors.blue};
  padding: 2%;
  height: 200px;
  margin-bottom: 30px;
`

export const Button = styled.a`
background : ${({ theme }) => theme.colors.blue};
  color: ${({ theme }) => theme.colors.white};
  border-radius: 4px;
  border: none;
  padding: .9rem 4.5rem;
  text-decoration: none;
`
export const BtnCase = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`
