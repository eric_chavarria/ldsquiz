import styled from 'styled-components'

export const Footer = styled.section`
  padding: 0 6%;
  height: 200px; 
  background: ${({ theme }) => theme.colors.blue};
  color: ${({ theme }) => theme.colors.white};
  align-items: center;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  @media (min-width: 768px) and (max-width: 991.98px) { 
  padding: 0;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    padding: 0;
    flex-direction: column;
    height: 320px;
    justify-content: flex-start;
   }
`
export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  width: 30%;
  height: 100px;
  align-content: flex-end; 
   @media (min-width: 992px) and (max-width: 1183px) { 
  }
   @media (min-width: 768px) and (max-width: 991.98px) { 
    align-content:center;
    width: 25%;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 100%;
    justify-content: center;
    align-content:center;
   }
`
export const ContainerData = styled(Container)`
  width:33%;
  @media (min-width: 768px) and (max-width: 991.98px) { 
    text-align:center;
    width: 20%;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 100%;
    
  }
`

export const Logos = styled(Container)` 
  width: 25%;
  justify-content: flex-between;
  @media (min-width: 992px) and (max-width: 1183px) { 
    width: 20%;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 100%;
    justify-content: space-evenly;
   }
`
export const Icon = styled.img`
height: 30px;
&:hover  {
    cursor: pointer;   
  } 
`

export const Logo = styled.img`
  position: relative;
  top 15px;
  height: 100%;
   &:hover  {
    cursor: pointer;   
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
    width: 100%;
    top: 0;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    Padding: 0 10% ; 
   }
`
export const H3 = styled.h3`
  font-family: 'Raleway';
  color: ${({ theme }) => theme.colors.orange};
  width: 100%;
  position: relative;
  @media (min-width: 768px) and (max-width: 991.98px) { 
   margin: 8px 0;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    text-align: center;
  }
`
export const Paragraph = styled.p` 
  font-family: 'Raleway';
  width: 40%;
  margin: 0;
  @media (min-width: 992px) and (max-width: 1183px) { 
   width: 35%;
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
   width: 100%;
   margin: 8px 0;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    text-align: center;
   }
`

export const Reserved = styled(Paragraph)`
  padding-bottom: 30px;
  width: 100%;
  font-size: 10px;
  text-align: center;
  background: ${({ theme }) => theme.colors.blue};
  color: ${({ theme }) => theme.colors.white};
`
