import Link from 'next/link'
import {
  Footer,
  Container,
  Logo,
  H3,
  Paragraph,
  Logos,
  Icon,
  Reserved,
  ContainerData
} from './styled'

export default function FooterSection () {
  return (
    <>
      <Footer>
        <Container>
          <Link href='/'>
            <Logo src='img/logo_foo.png' alt='Logo Kondosoft.' />
          </Link>
        </Container>
        <ContainerData>
          <H3>CONTACT US</H3>
          <Paragraph>hire@kondosoft.com</Paragraph>
          <Paragraph>993 117 54 35</Paragraph>
        </ContainerData>
        <Logos>
          <Icon alt='icon instagram' src='img\icon-instagram.svg' />
          <Icon alt='icon twitter' src='img\icon-twitter.svg' />
          <Icon alt='icon facebook' src='img\icon-facebook.svg' />
          <Icon alt='icon youtube' src='img\icon-youtube.svg' />
        </Logos>
      </Footer>
      <Reserved>Copyright 2019 Kondosoft All.rights reserved.</Reserved>
    </>
  )
}
