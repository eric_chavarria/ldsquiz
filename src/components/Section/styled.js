import styled from 'styled-components'

export const Container = styled.section`
  padding: 0 6%;
  background: linear-gradient(to bottom,${({ theme }) => theme.colors.blue} );
  color: ${({ theme }) => theme.colors.white};
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: center;
`

export const Paragraph = styled.div`
  color:  rgba(0, 66, 131, 0);
  font-family: 'Raleway';
  font-weight: 600;
  font-size:3rem;
  text-align: center;
  line-height: 470px;
  width: 100%;
  height: 100%;
  transition-property: background;
  transition-duration: .8s;
  &:hover  {
    background : rgba(0, 66, 131, 0.7);
    color: ${({ theme }) => theme.colors.white};
    cursor: pointer; 
  } 
  @media (min-width: 1184px) and (max-width: 1500px) {
    line-height: 270px;
  }
  @media (min-width: 992px) and (max-width: 1183px) { 
    line-height: 270px;
  }
  @media (min-width: 768px) and (max-width: 991.98px) {
    line-height: 270px;
  }
  @media (min-width: 576px) and (max-width: 767.98px) {    
    line-height: 480px;
  }
`

export const Title = styled.h2`
  color: ${({ theme }) => theme.colors.white};
  font-family: 'Raleway';
  margin:  50px 0;
  width: 100%;
  line-height: 80px;
  display: flex;
  justify-content: center;
`

export const CardContainer = styled.div`
  max-width: 20050px;
  display: grid;
  grid-template-columns: repeat(3, 500px);
  grid-template-rows: repeat(${props => props.size > 3 ? '2' : '1'}, 500px);
  @media (min-width: 1184px) and (max-width: 1500px) {
    max-width: 1450px;
    grid-template-columns: repeat(3, 300px);
    grid-template-rows: repeat(${props => props.size > 3 ? '2' : '1'}, 300px);
  }
  @media (min-width: 992px) and (max-width: 1183px) { 
    max-width: 1450px;
    grid-template-columns: repeat(3, 300px);
    grid-template-rows: repeat(${props => props.size > 3 ? '2' : '1'}, 300px);
  }
  @media (min-width: 768px) and (max-width: 991.98px) {
    max-width:700px;
    grid-template-columns: repeat(2, 345px);
    grid-template-rows: repeat(${props => props.size > 3 ? '2' : '1'}, 300px);
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
     max-width:500px;
    grid-template-columns: repeat(1, 480px);
    grid-template-rows: repeat(${props => props.size}, 480px);
  }
`

export const Card = styled.div`
  margin: 0 10px 20px 10px;
  background-image: url(${props => props.image});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  overflow: hidden;
  -webkit-box-shadow: 11px 9px 5px -6px rgba(0,0,0,0.32);
  -moz-box-shadow: 11px 9px 5px -6px rgba(0,0,0,0.32);
  box-shadow: 11px 9px 5px -6px rgba(0,0,0,0.32);
`
export const Logo = styled.img`
  whidth: 60px;
  height: 80px;
  padding-right: 10px;
  @media (min-width: 768px) and (max-width: 991.98px) {
     width: 50px;
     height: 75px;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    width: 45px;  
    height: 75px;
  }
`
