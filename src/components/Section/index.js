import {
  Container,
  Paragraph,
  Title,
  Logo,
  CardContainer,
  Card
} from './styled'

export default function Section ({ jdata, kid, onShowModal, ...props }) {
  return (
    <Container id={kid}>
      <CardContainer size={jdata.length}>
        {
          jdata.map(item => (
            <Card key={item.id} image={'fill2.jpg'} onClick={onShowModal(jdata[item.id])}>
              <Paragraph name={item.id}>
                {item.id}
              </Paragraph>
            </Card>
          ))
        }
      </CardContainer>
    </Container>
  )
}
