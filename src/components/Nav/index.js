import Link from 'next/link'
import {
  Nav,
  Img,
  LogoKondosoft,
  Container,
  Logos,
  Anchor
} from './styled'

export default function Navegation () {
  return (
    <Nav>
      <Logos>
        <Link href='/'>
          <LogoKondosoft src='img/principal-logo.png' alt='Logo Kondosoft.' />
        </Link>
      </Logos>
      <Container>
        <Link href='#MOBILE'><Anchor>MOBILE</Anchor></Link>
        <Link href='#WEB'><Anchor>WEB</Anchor></Link>
        <Link href='#GAMES'><Anchor>GAMES</Anchor></Link>
        <Link href='#HIRE'><Anchor>HIRE US</Anchor></Link>
      </Container>
      <Logos>
        <Img alt='icon facebook' src='img\icon-facebook.svg' />
        <Img alt='icon twitter' src='img\icon-twitter.svg' />
        <Img alt='icon instagram' src='img\icon-instagram.svg' />
        <Img alt='icon youtube' src='img\icon-youtube.svg' />
      </Logos>
    </Nav>
  )
}
