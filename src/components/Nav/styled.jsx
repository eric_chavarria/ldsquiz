import styled from 'styled-components'

export const Nav = styled.nav`
  height: 90px;
  background : ${({ theme }) => theme.colors.blue};
  color: ${({ theme }) => theme.colors.white};;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: fixed;
  width: 88%;
  top: 0;
  padding: 0 6%;
  z-index: 3;
  @media (min-width: 768px) and (max-width: 991.98px) { 
    width: 100%;
    padding: 0;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    padding: 0;
    width: 100%;
  }
`
  
export const Img = styled.img`
  width: auto;
  height: 30px;
  &:hover  {
    cursor: pointer;   
  } 
`

export const LogoKondosoft = styled.img`
  height: 65px;
  @media (min-width: 768px) and (max-width: 991.98px) { 
    height: 50px;
  }
`
export const Container = styled.div`
  padding-right: 5%;
  display: flex;
  justify-content: space-between;
  width: 40%;
  @media (min-width: 992px) and (max-width: 1183px) { 
    padding-right: 0%;
    width: 40%;
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
    padding-right: 0%;
    width: 50%;
  }
  @media (min-width: 576px) and (max-width: 767.98px) { 
    padding-right: 0%;
    margin: 0 2%;
    width: 1000%;
  }
`
export const Logos = styled(Container)`
  padding: 0;
  width: 15%;
  justify-content: flex-between;
  &:hover  {
    cursor: pointer;   
  } 
  @media (min-width: 992px) and (max-width: 1183px) { 
   width: 20%;
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
    width: 23%;
    margin: 10px;
  }
    @media (min-width: 576px) and (max-width: 767.98px) { 
    display:none;
   }
`

export const Anchor = styled.a`
  font-family: 'Raleway';
  font-size: 1.2rem;
  padding: 10px; 
  border: 3px solid  ${({ theme }) => theme.colors.blue};
  &:hover  {
    border: 3px solid  ${({ theme }) => theme.colors.orange};
    cursor: pointer;   
  } 
   @media (min-width: 992px) and (max-width: 1183px) { 
   font-size: 1rem;
  }
  }   
`
