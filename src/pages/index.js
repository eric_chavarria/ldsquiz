import { useState, useCallback } from 'react'
import Section from '../components/Section/index'
import Modal from '../components/Modal/index'
import {
  Bullet,
  Projects
} from '../_styled'

function Home (Info) {
  const [numImage, setNumImage] = useState(0)
  const [hidden, setHiden] = useState('hidden')
  const [element, setElement] = useState('')
  const click = useCallback((number) => {
    return () => {
      setHiden('visible')
      setElement(number)
    }
  }, [])
  function closeModal () {
    setElement('')
    setHiden('hidden')
    setNumImage(0)
  }
  
  return (
    <>
      
      <Projects>
        <Bullet>Preguntas</Bullet>
        <Section onShowModal={click} jdata={Info.Info.MOBILE} kid='MOBILE' />
        <Modal numImage={numImage} setNumImage={setNumImage} visible={hidden} selectedProjectId={element} onCloseModal={closeModal} />
      </Projects>
    
    </>
  )
}

export default Home
export async function getStaticProps () {
  const res = await fetch('http://localhost:3000/api/info-pages')
  const Info = await res.json()
  return {
    props: {
      Info
    }
  }
}
